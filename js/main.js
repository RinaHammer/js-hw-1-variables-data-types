//1. Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name. Скопіюйте це значення в змінну admin і виведіть його в консоль.
//ДО УВАГИ МЕНТОРА: Visual Code написав мені помилку щодо змінної name, типу це зарезервоване слово, тож я використовую змінну myName. (Ось що він написав: 'name' is deprecated.ts(6385) lib.dom.d.ts(27028, 5): The declaration was marked as deprecated here.) const name: void)

let admin;
let myName = "Iryna";
admin = myName;
console.log(admin);

//2. Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і виведіть на консоль.

let days = 2;
let secоnds = days * 24 * 60 * 60;
console.log(secоnds);

//3. Запитайте у користувача якесь значення і виведіть його в консоль.

let favoriteDrink = prompt("Напиши назву свого улюбленого напою?", "Slurm");
console.log(favoriteDrink);
